all:
	gcc main.c procfs_browser.c cnxinfo.c -lpcap $(shell pkg-config --cflags --libs glib-2.0 neo4j-client) -g -ggdb -W -Wall -Wextra

start_neo4j:
	sudo systemctl start docker
	sudo docker-compose -f docker-compose.yml up -d

run:
	sudo ./a.out
