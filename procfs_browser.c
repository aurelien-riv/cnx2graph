#include "procfs_browser.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <netinet/in.h>

unsigned int get_inode_by_port_proto(unsigned int port, const char *proto)
{
    char filename[16];
    strcpy(filename, "/proc/net/");
    strncat(filename, proto, 3);

    FILE *proc_net_proto = fopen(filename, "r");
    char line[1024];
    while (fgets(line, 1024, proc_net_proto))
    {
        unsigned long loc_port, inode;
        if (sscanf(line, "%*u: %*x:%lX %*x:%*x %*x %*x:%*x %*x:%*x %*x %*d %*d %lu", &loc_port, &inode) != 2)
            continue;
        if (port == ntohs(loc_port))
        {
            fclose(proc_net_proto);
            return inode;
        }
    }
    fclose(proc_net_proto);
    return 0;
}

unsigned int get_pid_by_inode(unsigned int inode)
{
    DIR *procfsdir = opendir("/proc/");
    struct dirent *d1;

    while ((d1 = readdir(procfsdir)))
    {
        unsigned int pid;
        char crap;
        // only open dirs with a pid for name
        if (sscanf(d1->d_name, "%d%c", &pid, &crap) != 1)
            continue;

        char fdDirPath[16];
        sprintf(fdDirPath, "/proc/%u/fd/", pid);

        DIR *piddir = opendir(fdDirPath);
        struct dirent *d2;
        while ((d2 = readdir(piddir)))
        {
            char fdFilePath[32];
            snprintf(fdFilePath, 31, "%s%s", fdDirPath, d2->d_name);

            char lnk[64];
            if (! readlink(fdFilePath, lnk, sizeof(lnk)-1))
                continue;

            unsigned int ino;
            if (sscanf(lnk, "socket:[%u]", &ino) == 1 && ino == inode)
            {
                closedir(piddir);
                closedir(procfsdir);
                return pid;
            }
        }
        closedir(piddir);
    }
    closedir(procfsdir);
    return 0;
}

unsigned int get_pid_by_port_proto(unsigned int port, const char *proto)
{
    unsigned int inode = get_inode_by_port_proto(port, proto);
    if (! inode)
        return 0;
    return get_pid_by_inode(inode);
}

char *get_cmdline_for_pid(unsigned int pid)
{
    char path[32];
    sprintf(path, "/proc/%u/cmdline", pid);
    FILE *file = fopen(path, "r");

    char line[1024];
    fgets(line, 1024, file);

    fclose(file);

    return strdup(line);
}

void get_pid_info(unsigned int pid, processinfo *process)
{
    char path[32];
    sprintf(path, "/proc/%u/status", pid);
    FILE *statusFile = fopen(path, "r");

    char line[1024];
    while (fgets(line, 1024, statusFile))
    {
        char key[32];
        sscanf(line, "%s: ", key);
        if (strcmp(key, "Name:") == 0)
        {
            char value[32];
            sscanf(line, "%*s %s", value);
            process->name = strdup(value);
        }
        else if (strcmp(key, "Uid:") == 0)
        {
            // we only use uid and euid for now
            unsigned int real_uid, effective_uid, saved_set_uid, fs_uid;
            sscanf(line, "%*s %u %u %u %u", &real_uid, &effective_uid, &saved_set_uid, &fs_uid);
            process->owner = userinfo_new_from_uid(real_uid);

            if (effective_uid != real_uid)
                process->effective_owner = userinfo_new_from_uid(effective_uid);
        }
    }
    fclose(statusFile);

    process->cmdline = get_cmdline_for_pid(pid);
}
