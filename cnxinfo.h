#pragma once

#include <time.h>

typedef struct userinfo {
    char *name;
    unsigned int uid;
    char *remote_srv;
} userinfo;

typedef struct processinfo {
    char *name;
    char *cmdline;
    struct userinfo *owner, *effective_owner;
} processinfo;

typedef struct cnxinfo {
    char src_ip[32], 
     dst_ip[32];
    unsigned int dst_port;
    time_t ts;

    processinfo *process;
} cnxinfo;

userinfo *userinfo_new_from_uid(unsigned int uid);

processinfo *processinfo_new();

cnxinfo *cnxinfo_new ();
cnxinfo *cnxinfo_new_from_ips_and_port(char *src_ip, char *dst_ip, unsigned int dst_port);
/** Sync the stucture with the neo4j database (TODO) */
int       cnxinfo_send();
void      cnxinfo_free();
