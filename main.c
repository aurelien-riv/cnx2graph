#include <stdio.h>
#include <pcap.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h> 

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#include <glib.h>

#include "structs/ethernet.h"
#include "structs/ip.h"
#include "structs/tcp.h"
#include "procfs_browser.h"
#include "cnxinfo.h"

char *get_default_device()
{
    char errbuf[PCAP_ERRBUF_SIZE];
    char *dev = pcap_lookupdev(errbuf);

    if (dev == NULL) {
            fprintf(stderr, "Couldn't find default device: %s\n", errbuf);
            exit(2);
    }
    return dev;
}

char *get_local_ipv4_from_interface_name(char *iface_name)
{
    int fd;
    struct ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;

    strncpy(ifr.ifr_name, iface_name, IFNAMSIZ-1);

    ioctl(fd, SIOCGIFADDR, &ifr);

    close(fd);

    return inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
}

pcap_t *init_pcap(char *dev, char *filter_exp)
{
    struct bpf_program fp;
    char errbuf[PCAP_ERRBUF_SIZE];
    bpf_u_int32 net = 0;

    pcap_t *handle = pcap_open_live(dev, BUFSIZ, 0, 1000, errbuf);
    if (handle == NULL) {
        fprintf(stderr, "Couldn't open device %s: %s\n", dev, errbuf);
        exit(2);
    }
    if (pcap_compile(handle, &fp, filter_exp, 0, net) == -1) {
        fprintf(stderr, "Couldn't parse filter %s: %s\n", filter_exp, pcap_geterr(handle));
        exit(2);
    }
    if (pcap_setfilter(handle, &fp) == -1) {
        fprintf(stderr, "Couldn't install filter %s: %s\n", filter_exp, pcap_geterr(handle));
        exit(2);
    }
    return handle;
}

void parse_http_request(G_GNUC_UNUSED const unsigned char *payload)
{
    // TODO
}

void on_capture(unsigned char *user_data, G_GNUC_UNUSED const struct pcap_pkthdr *header, const unsigned char *packet)
{
    GHashTable *cnxStore = (GHashTable *) user_data;

    if (! packet)
    {
        fprintf(stderr, "Null packet!\n");
        return;
    }

    const struct sniff_ip *ip = (struct sniff_ip*)(packet + SIZE_ETHERNET);
    u_int size_ip = IP_HL(ip)*4;
    if (size_ip < 20) 
        return;
    
    const struct sniff_tcp *tcp = (struct sniff_tcp*)(packet + SIZE_ETHERNET + size_ip);
    uint size_tcp = TH_OFF(tcp)*4;
    if (size_tcp < 20)
        return;

    if (tcp->th_flags & TH_SYN)
    {
        uint pid = get_pid_by_port_proto(tcp->th_sport, "tcp");
        
        char src[32], dst[32];
        inet_ntop(AF_INET, &(ip->ip_src), src, INET6_ADDRSTRLEN);
        inet_ntop(AF_INET, &(ip->ip_dst), dst, INET6_ADDRSTRLEN);

        cnxinfo *cnx = cnxinfo_new_from_ips_and_port(src, dst, ntohs(tcp->th_dport));
        cnx->process = processinfo_new();
        get_pid_info(pid, cnx->process);

        g_hash_table_insert(cnxStore, GUINT_TO_POINTER(tcp->th_sport), cnx);
    }
    else if (tcp->th_flags & TH_PUSH)
    {
        const unsigned char *payload = (u_char *)(packet + SIZE_ETHERNET + size_ip + size_tcp);
        parse_http_request(payload);
    }
    else if (tcp->th_flags & TH_FIN)
    {
        cnxinfo *cnx = g_hash_table_lookup(cnxStore, GUINT_TO_POINTER(tcp->th_sport));
        if (! cnx)
        {
            fprintf(stderr, "FIN without SYNC, or storage failure\n");
            return;
        }

        cnxinfo_send(cnx);

        g_hash_table_remove(cnxStore, GUINT_TO_POINTER(tcp->th_sport));
    }
    else
    {
    }

}
int main(void)
{
    char *dev = get_default_device();
    char *ip = get_local_ipv4_from_interface_name(dev);

    char filter_exp[52];
    strcpy(filter_exp, "src host ");
    strcat(filter_exp, ip);

    printf("Device: %s\n", dev);

    GHashTable *cnxStore = g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL, cnxinfo_free);

    pcap_t *handle = init_pcap(dev, filter_exp);
    pcap_loop(handle, -1, on_capture, (u_char *) cnxStore);

    pcap_close(handle);
    g_hash_table_destroy(cnxStore);

    return(0);
}
