#include "cnxinfo.h"
#include <stdlib.h>
#include <string.h>

//region user
#include <pwd.h>
#include <utmp.h>

userinfo *userinfo_new()
{
    userinfo *self = (userinfo *) malloc(sizeof(userinfo));
    self->name       = NULL;
    self->remote_srv = NULL;
    return self;
}


userinfo *userinfo_new_from_uid(unsigned int uid)
{
    userinfo *self = userinfo_new();
    self->uid  = uid;

    struct passwd *passwd_entry = getpwuid(uid);
    self->name = strdup(passwd_entry->pw_name);
    
    struct utmp *utmp_data;
    utmpname(UTMP_FILE);
    setutent();
    while ((utmp_data = getutent()))
    {
        if (! strcmp(self->name, utmp_data->ut_user))
        {
            self->remote_srv = strdup(utmp_data->ut_host);
            break;
        }
    }

    return self;
}

void userinfo_free(userinfo *self)
{
    if (self->remote_srv)
        free(self->remote_srv);
    free(self->name);
    free(self);
}
//endregion user

processinfo *processinfo_new()
{
    processinfo *self = (processinfo *) malloc(sizeof(processinfo));
    self->name            = NULL;
    self->cmdline         = NULL;
    self->owner           = NULL;
    self->effective_owner = NULL;
    return self;
}
void processinfo_free(processinfo *self)
{
    if (self->owner)
        userinfo_free(self->owner);
    if (self->effective_owner)
        userinfo_free(self->effective_owner);
    free(self->name);
    free(self->cmdline);
    free(self);
}

cnxinfo *cnxinfo_new()
{
    cnxinfo *self = (cnxinfo *) malloc(sizeof(cnxinfo));
    self->process = NULL;
    return self;
}

cnxinfo *cnxinfo_new_from_ips_and_port(char *src_ip, char *dst_ip, unsigned int dst_port)
{
     cnxinfo *self = cnxinfo_new();
     strcpy(self->src_ip, src_ip);
     strcpy(self->dst_ip, dst_ip);
     self->dst_port = dst_port;
     return self;
}

#include <neo4j-client.h>
#include <errno.h>
// FIXME temporary, this is a violation in the separation of concerns principle
int cnxinfo_send(cnxinfo *self)
{

    neo4j_client_init();
    neo4j_connection_t *connection = neo4j_connect("neo4j://neo4j:passw0rd@localhost:7687", NULL, NEO4J_INSECURE);

    if (connection == NULL)
    {
        neo4j_perror(stderr, errno, "Connection failed");
        return EXIT_FAILURE;
    }

    char query[1024], buf[16];
    neo4j_result_stream_t *results;
    neo4j_result_t *result;
    neo4j_value_t value;

#define RUN(query)                                                              \
    puts(query);                                                                \
    results = neo4j_run(connection, query, neo4j_null);                         \
    if (results == NULL)                                                        \
    {                                                                           \
        neo4j_perror(stderr, errno, "Failed to run statement");                 \
        return EXIT_FAILURE;                                                    \
    }                                                                           \
    result = neo4j_fetch_next(results);                                         \
    if (result == NULL)                                                         \
    {                                                                           \
        neo4j_perror(stderr, errno, "Failed to fetch result");                  \
        return EXIT_FAILURE;                                                    \
    }                                                                           \

#define RUN_FETCH_ID(query, nodeId)                                             \
    RUN(query)                                                                  \
    value = neo4j_result_field(result, 0);                                      \
    neo4j_tostring(value, buf, sizeof(buf));                                    \
    nodeId = atoi(buf)

    int loId, remoteId, appId, userId, cnxId;

    sprintf(query, "MERGE (lo:Server {ip: '%s'}) RETURN id(lo)", self->src_ip);
    RUN_FETCH_ID(query, loId);

    sprintf(query, "MERGE (remote:Server {ip: '%s'}) RETURN id(remote)", self->dst_ip);
    RUN_FETCH_ID(query, remoteId);

    processinfo *process = self->process;
    sprintf(query, "MERGE (app:Application {name: '%s', cmdline: '%s'}) RETURN id(app)", process->name, process->cmdline);
    RUN_FETCH_ID(query, appId);
    
    userinfo *owner = process->owner;
    sprintf(query, "MERGE (user:User {name: '%s', uid: %d}) RETURN id(user)", owner->name, owner->uid);
    RUN_FETCH_ID(query, userId);
    
    // TODO use SYN timestamp (and FIN timestamp for duration)
    sprintf(query, "MERGE (cnx:Connection {ts: %lld, duration: %u, proto: '%s', port: %u}) RETURN id(cnx)", (long long) time(NULL), 0, "tcp", self->dst_port);
    RUN_FETCH_ID(query, cnxId);

    sprintf(query, 
            "MATCH (lo), (remote), (app), (user), (cnx) "
            "WHERE id(lo)=%u AND id(remote)=%u AND id(app)=%u AND id(user)=%u AND id(cnx)=%u "
            "CREATE (lo)-[:ESTABLISHES]->(cnx), (cnx)-[:TOWARDS]->(remote), (cnx)-[:FROM_APP]->(app), (cnx)-[:FROM_USER]->(user) "
            "RETURN null",
            loId, remoteId, appId, userId, cnxId);
    RUN(query);

    userinfo *effective_owner = process->effective_owner;
    if(effective_owner)
    {
        int effective_userId;
        sprintf(query, "MERGE (user:User {name: '%s', uid: %d}) RETURN id(user)", effective_owner->name, effective_owner->uid);
        RUN_FETCH_ID(query, effective_userId);

        sprintf(query, 
            "MATCH (user), (cnx) "
            "WHERE id(user)=%u AND id(cnx)=%u "
            "CREATE (cnx)-[:FROM_EFFECTIVE_USER]->(user) "
            "RETURN null",
            effective_userId, cnxId);
        RUN(query);
    }

    if (owner->remote_srv)
    {
        int srvId;
        sprintf(query, "MERGE (srv:Server {hostname: '%s'}) RETURN id(srv)", owner->remote_srv);
        RUN_FETCH_ID(query, srvId);

        sprintf(query, 
                "MATCH (srv), (cnx) "
                "WHERE id(srv)=%d AND id(cnx)=%d "
                "CREATE (cnx)-[:FROM_REMOTE]->(srv) "
                "RETURN null",
                srvId, cnxId);
        RUN(query);

    }
    neo4j_close(connection);
    neo4j_client_cleanup();

    return 0;
}

void cnxinfo_free(cnxinfo *self)
{
    if (self->process)
        processinfo_free(self->process);
    free(self);
}
