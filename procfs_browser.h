#pragma once

#include "cnxinfo.h"

unsigned int get_inode_by_port_proto(unsigned int port, const char *proto);
unsigned int get_pid_by_inode       (unsigned int inode);
unsigned int get_pid_by_port_proto  (unsigned int port, const char *proto);
void         get_pid_info           (unsigned int pid, processinfo *process);
